#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecure.h>
#include <Losant.h>
#include <ArduinoJson.h>

#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

Adafruit_BME280 bme;

float temperature, humidity, pressure, altitude;

const char* WIFI_SSID = "SSID";
const char* WIFI_PASS = "PASSWORD";

const char* LOSANT_DEVICE_ID = "ID";
const char* LOSANT_ACCESS_KEY = "KEY";
const char* LOSANT_ACCESS_SECRET = "SECRET";

const int analogInPin = A0;
double valueVoltage = 0;

WiFiClient wifiClient;

LosantDevice device(LOSANT_DEVICE_ID);

void setup()
{
  Serial.begin(115200);
  Serial.println();

  readVoltage();

  connectWifi();

  checkLosantData();

  connectLosant();

  bme.begin(0x76); 

  readSendData();

  for (int i = 0 ; i < 5 ; i++){
    device.loop();
    delay(500);  
  }

  Serial.println("Going into deep sleep for 5 Minute");
  ESP.deepSleep(300e6);
}


void loop() {
  
  }

void readVoltage(){
  double temp = 3.2 / 1024;
  int sensorValueVoltage = analogRead(analogInPin);
  valueVoltage = sensorValueVoltage * temp;
}

void connectWifi(){
  WiFi.begin(WIFI_SSID, WIFI_PASS);

  Serial.print("Connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }
  Serial.println();

  Serial.print("Connected, IP address: ");
  Serial.println(WiFi.localIP());
}

void checkLosantData(){
  HTTPClient http;
  http.begin("http://api.losant.com/auth/device");
  http.addHeader("Content-Type", "application/json");
  http.addHeader("Accept", "application/json");

  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["deviceId"] = LOSANT_DEVICE_ID;
  root["key"] = LOSANT_ACCESS_KEY;
  root["secret"] = LOSANT_ACCESS_SECRET;
  String buffer;
  root.printTo(buffer);
  
  int httpCode = http.POST(buffer);

  if (httpCode > 0) {
    if (httpCode == HTTP_CODE_OK) {
      Serial.println("This device is authorized!");
    } else {
      Serial.println("Failed to authorize device to Losant.");
      if (httpCode == 400) {
        Serial.println("Validation error: The device ID, access key, or access secret is not in the proper format.");
      } else if (httpCode == 401) {
        Serial.println("Invalid credentials to Losant: Please double-check the device ID, access key, and access secret.");
      } else {
        Serial.println("Unknown response from API");
      }
      Serial.println("Current Credentials: ");
      Serial.println("Device id: ");
      Serial.println(LOSANT_DEVICE_ID);
      Serial.println("Access Key: ");
      Serial.println(LOSANT_ACCESS_KEY);
      Serial.println("Access Secret: ");
      Serial.println(LOSANT_ACCESS_SECRET);
      return;
    }
  } else {
    Serial.println("Failed to connect to Losant API.");
    return;
  }

  http.end();
}

void connectLosant(){
    Serial.println("Connecting to Losant...");

  Serial.println("Authenticating Device...");

  

  device.connect(wifiClient, LOSANT_ACCESS_KEY, LOSANT_ACCESS_SECRET);
  
  while (!device.connected()) {
    delay(1000);
    Serial.println(device.mqttClient.state());
  }

  Serial.println("Connected!");
  Serial.println("This device is now ready for use!");
}

void readSendData(){
  temperature = bme.readTemperature();
  humidity = bme.readHumidity();
  
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["temp"] = temperature;
  root["hum"] = humidity;
  root["voltage"] = valueVoltage;
  device.sendState(root);
  Serial.println("Die Daten wurden gesendet");
}
